function update() {
  updateGame();
  updateGraphics();
  window.requestAnimationFrame(update);
}

function preventGestures(element) {
  const stop = (e) => {
    const tag = e.target.tagName.toLowerCase();
    if (tag === 'textarea' || tag === 'input') return;
    e.preventDefault();
  };

  const prevent = e => element.addEventListener(e, stop, { passive: false });
  prevent('touchstart');
  prevent('touchmove');
  prevent('touchend');
}

function init() {
  $('.msg').addEventListener('touchstart', restart);
  $('.close').ontouchend = hideDialog;
  $('.save').ontouchend = saveMyScore;
  $('.score').ontouchend = onScoreTap;
  preventGestures(document.body);
  update();
  updateHighScore();
}

window.onload = init;
