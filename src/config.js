const config = {
  gravity: 0, // 800,
  initVy: -500, // -1300,
  vyIncreasePerRound: 15,
  balloonYDist: 600, // max dist btw balloons in group when created
  minVx: -200,
  maxVx: 200,
  maxMisses: 3,
  targetHeight: 200,
  rotationFactor: 0.7,
  scorePerSmash: 1000,
  stepsBtwRounds: 200,
  maxObjects: 6,
  bombOdds: 0.3,
  demoOdds: 0.005,
  hitSound: './assets/pop.wav',
  missSound: './assets/wrong.wav',
  audio: false,
};
