const elements = {};

const $ = sel => document.querySelector(sel);

const hitSound = new Audio(config.hitSound);
const missSound = new Audio(config.missSound);

function play(sound) {
  if (config.audio) sound.play();
}

function showDialog({ isHighScore, score }) {
  $('.overlay').style.display = 'flex';
  $('.save-form').style.display = isHighScore ? 'block' : 'none';
  $('.final-score').innerText = score;
}

function updateHighScore() {
  const current = getScore();
  $('.highscore').innerText = current ? `${current.name}: ${current.score}p` : 'No scores yet';
}

function hideDialog() {
  $('.overlay').style.display = 'none';
  updateHighScore();
  clearBoard();
}

function saveMyScore() {
  const name = $('.name').value;
  if (name) {
    saveScore(name, context.score);
    hideDialog();
  }
}

function clearBoard() {
  $('.score').innerText = `Score: 0`;
  $('.misses').innerText = `Misses: 0`;
}

function onEvent(type, data) {
  const board = $('.board');
  if (type === 'newObject') {
    const obj = document.createElement('div');
    obj.className = data.className;
    obj.addEventListener('touchstart', () => hit(data.id, data));
    board.appendChild(obj);
    elements[data.id] = obj;
  }
  else if (type === 'removeObject') {
    board.removeChild(elements[data.id]);
  }
  else if (type === 'objectMissed') {
    $('.misses').innerText = `Misses: ${context.misses}`;
    play(missSound);
  }
  else if (type === 'smashObject') {
    $('.score').innerText = `Score: ${context.score}`;
    play(hitSound);
  }
  else if (type === 'gameOver') {
    stop();
    const isHighScore = isNewHighScore(context.score);
    showDialog({ isHighScore, score: context.score });
    $('.msg').style.display = 'block';
  }
  else if (type === 'restart') {
    clearBoard();
    $('.msg').style.display = 'none';
  }
}

let lastTap, tap; // hack to reset highscore
function onScoreTap() {
  const now = new Date().getTime();
  const tapTime = 1000;
  if (tap === 4 && !context.running) {
    resetHighScore();
    updateHighScore();
  }
  else if (!lastTap || now - lastTap > tapTime) {
    lastTap = now;
    tap = 0;
  }
  else tap++;
}

function updateGraphics() {
  context.gameObjects.forEach((o) => {
    const x = o.x;
    const y = o.y;
    const pos = `translate3d(${x}px, ${y}px, 0) rotate(${o.angle}deg)`;
    // console.log(y);
    const el = elements[o.id];
    el.style.transform = pos;
  });
}
