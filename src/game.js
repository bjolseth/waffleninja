const Height = 1080;
const Width = 1920;

// coord system follows normal DOM x,y
// meaning origo in top left corner, positive to the right and down

let fps = 60;

const context = {
  nextId: 0,
  gameObjects: [],
  misses: 0,
  step: 0,
  score: 0,
  running: false,
  round: 0,
};

function rand(min, max) {
  return min + Math.random() * (max - min);
}

function checkObjects() {
  context.gameObjects = context.gameObjects.filter((o) => {
    const wentOutside = (o.y > Height && o.vy > 0) || o.y < -config.targetHeight;
    if (wentOutside || o.smashed) {
      if (o.type === 'target' && wentOutside && context.running) missed();
      onEvent('removeObject', o);
      return false;
    }
    return true;
  });
}

function gameOver() {
  onEvent('gameOver');
  context.running = false;
}

function missed() {
  context.misses++;
  onEvent('objectMissed');
  if (context.misses >= config.maxMisses) gameOver();
}

function updateObjects() {
  context.gameObjects.forEach((o) => {
    o.x += o.vx / fps;
    const yAccel = config.gravity;
    o.vy += + yAccel / fps;
    o.y += o.vy / fps;
    o.angle += o.rotation / fps;
  });
}

function randCol() {
  const colors = ['green', 'blue'];
  return colors[Math.floor(rand(0, colors.length))];
}

function addObjects() {
  context.round++;

  // stack below balloons, so its not too easy to hit by accident
  const addBomb = Math.random() < config.bombOdds;
  if (addBomb) addObject(context.round, 'bomb', 'bomb');

  const objects = Math.min(config.maxObjects, context.round);
  for (let i = 0; i < objects; i++) {
    const color = randCol();
    const className = 'target balloon-' + color;
    addObject(context.round, 'target', className);
  }
}

function updateGame() {
  context.step++;
  updateObjects();
  checkObjects();
  if (context.running) {
    if (context.step % config.stepsBtwRounds === 0) addObjects();
  }
  else {
    if (Math.random() < config.demoOdds) {
      const className = 'target balloon-' + randCol();
      addObject(1, 'target', className);
    }
  }
}

function hit(id, data) {
  if (!context.running) return;
  if (data.type === 'bomb') {
    data.smashed = true;
    gameOver();
  }
  else if (data.type === 'target') {
    const o = context.gameObjects.find(o => o.id === id);
    o.smashed = true;
    context.score += config.scorePerSmash;
    checkObjects();
    onEvent('smashObject', o);
  }
}

function addObject(round, type, className) {
  let vx = rand(config.minVx, config.maxVx);
  const x = rand(Width / 2 - 600, Width / 2 + 600);

  let rotation = 0;
  if (type === 'bomb') {
    vx *= 2;
    rotation = vx * config.rotationFactor;
  }

  const y = Height + rand(10, config.balloonYDist);

  const vyInc = config.vyIncreasePerRound;
  const vy = config.initVy - round * vyInc - rand(-vyInc, vyInc);

  const object = {
    id: context.nextId,
    type,
    x,
    y,
    vy,
    vx,
    angle: 0,
    rotation,
    className,
  };
  context.gameObjects.push(object);
  context.nextId += 1;
  onEvent('newObject', object);
}

function restart() {
  context.gameObjects.forEach(o => onEvent('removeObject', o));
  context.running = true;
  context.gameObjects = [];
  context.misses = 0;
  context.step = 0;
  context.score = 0;
  context.round = 0;
  onEvent('restart');
  setTimeout(addObjects, 300);
}

// for debug
window.onkeydown = ({key}) => {
  if (key === 'p') context.running = !context.running;
  if (key === 's') fps = fps > 60 ? 60 : 120;
}
